import { defineConfig, presetIcons, presetUno } from "unocss";

export default defineConfig({
  theme: {
    colors: {
      primary: "rgba(62, 56, 130, 1)",
      secondary: "rgba(246, 245, 253, 1)",
      accent: "rgba(62, 56, 130, 1)",
      success: "rgba(18, 129, 73, 1)",
      warning: "rgba(134, 103, 27, 1)",
      error: "rgba(213, 27, 56, 1)",
      dark: "rgba(56, 56, 56, 1)",
    },
  },
  presets: [
    presetUno(),
    presetIcons({
      scale: 1.2,
    }),
  ],
});
