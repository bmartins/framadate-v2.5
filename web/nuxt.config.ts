// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['@unocss/nuxt', '@nuxtjs/i18n'],
  css: ['@unocss/reset/tailwind.css'],
  i18n: {
    strategy: 'no_prefix',
    lazy: true,
    langDir: 'locales',
    vueI18n: './i18n.config.ts',
    locales: [
      {
        code: 'en',
        iso: 'en',
        name: 'English',
        file: 'en.json',
        isCatchallLocale: true,
      },
      {
        code: 'fr',
        iso: 'fr',
        name: 'Français',
        file: 'fr.json',
      },
    ],
    defaultLocale: 'en',
    customRoutes: 'config',
    detectBrowserLanguage: {
      useCookie: false,
      redirectOn: 'root',
    },
  },
})
