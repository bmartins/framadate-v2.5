


## Run the project

```bash
  cp .env.example .env
  docker-compose up
```

- http://localhost:3000 (Frontend)
- http://localhost:3001 (Backend)
- http://localhost:5454 (DB)